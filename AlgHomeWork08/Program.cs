﻿using System.Diagnostics;

var array_1_100 = new int[100];
var array_2_100 = new int[100];
var array_1_1000 = new int[1000];
var array_2_1000 = new int[1000];
var array_1_10_000 = new int[10_000];
var array_2_10_000 = new int[10_000];
var array_1_100_000 = new int[100_000];
var array_2_100_000 = new int[100_000];
var array_1_1000_000 = new int[1000_000];
var array_2_1000_000 = new int[1000_000];
var array_1_10_000_000 = new int[10_000_000];
var array_2_10_000_000 = new int[10_000_000];
var array_1_100_000_000 = new int[100_000_000];
var array_2_100_000_000 = new int[100_000_000];

for (int i = 0; i < 100; i++)
{
    var number = new Random().Next(0, 100);
    array_1_100[i] = number;
    array_2_100[i] = number;
}

for (int i = 0; i < 1000; i++)
{
    var number = new Random().Next(0, 1000);
    array_1_1000[i] = number;
    array_2_1000[i] = number;
}


for (int i = 0; i < 10_000; i++)
{
    var number = new Random().Next(0, 10_000);
    array_1_10_000[i] = number;
    array_2_10_000[i] = number;
}

for (int i = 0; i < 100_000; i++)
{
    var number = new Random().Next(0, 100_000);
    array_1_100_000[i] = number;
    array_2_100_000[i] = number;
}

for (int i = 0; i < 1000_000; i++)
{
    var number = new Random().Next(0, 1000_000);
    array_1_1000_000[i] = number;
    array_2_1000_000[i] = number;
}

for (int i = 0; i < 10_000_000; i++)
{
    var number = new Random().Next(0, 10_000_000);
    array_1_10_000_000[i] = number;
    array_2_10_000_000[i] = number;
}

for (int i = 0; i < 100_000_000; i++)
{
    var number = new Random().Next(0, 100_000_000);
    array_1_100_000_000[i] = number;
    array_2_100_000_000[i] = number;
}
Stopwatch stopwatch = new Stopwatch();

stopwatch.Start();
QuickSort(array_1_100);
stopwatch.Stop();
long elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 100 - {0}", elapsedTime);

stopwatch.Restart();
QuickSort(array_1_1000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 1000 - {0}", elapsedTime);

stopwatch.Restart();
QuickSort(array_1_10_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 10 000 - {0}", elapsedTime);

stopwatch.Restart();
QuickSort(array_1_100_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 100 000 - {0}", elapsedTime);

stopwatch.Restart();
QuickSort(array_1_1000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 1 000 000 - {0}", elapsedTime);

stopwatch.Restart();
QuickSort(array_1_10_000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 10 000 000 - {0}", elapsedTime);

stopwatch.Restart();
QuickSort(array_1_100_000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("QuickSort для 100 000 000 - {0}", elapsedTime);


stopwatch.Restart();
MergeSort(array_2_100);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 100 - {0}", elapsedTime);

stopwatch.Restart();
MergeSort(array_2_1000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 1000 - {0}", elapsedTime);

stopwatch.Restart();
MergeSort(array_2_10_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 10 000 - {0}", elapsedTime);

stopwatch.Restart();
MergeSort(array_2_100_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 100 000 - {0}", elapsedTime);

stopwatch.Restart();
MergeSort(array_2_1000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 1 000 000 - {0}", elapsedTime);

stopwatch.Restart();
MergeSort(array_2_10_000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 10 000 000 - {0}", elapsedTime);

stopwatch.Restart();
MergeSort(array_2_100_000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("MergeSort для 100 000 000 - {0}", elapsedTime);

Console.ReadKey();


// Quick sort
void QuickSort(int[] arr)
{
    QuickSortAlgorithm(arr, 0, arr.Length - 1);
}

void QuickSortAlgorithm(int[] arr, int left, int right)
{
    if (left < right)
    {
        int pivotIndex = Partition(arr, left, right);
        QuickSortAlgorithm(arr, left, pivotIndex - 1);
        QuickSortAlgorithm(arr, pivotIndex + 1, right);
    }
}

int Partition(int[] arr, int left, int right)
{
    int pivot = arr[right];
    int i = left - 1;

    for (int j = left; j < right; j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            Swap(arr, i, j);
        }
    }

    Swap(arr, i + 1, right);
    return i + 1;
}

void Swap(int[] arr, int i, int j)
{
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

// Merge sort 
static void MergeSort(int[] array)
{
    MergeSortAlgorithm(array, 0, array.Length - 1);
}

static void Merge(int[] array, int left, int middle, int right)
{
    int n1 = middle - left + 1;
    int n2 = right - middle;

    int[] leftArray = new int[n1];
    int[] rightArray = new int[n2];

    for (int x = 0; x < n1; x++)
    {
        leftArray[x] = array[left + x];
    }
    for (int y = 0; y < n2; y++)
    {
        rightArray[y] = array[middle + 1 + y];
    }

    int i = 0;
    int j = 0;
    int k = left;
    while (i < n1 && j < n2)
    {
        if (leftArray[i] <= rightArray[j])
        {
            array[k] = leftArray[i];
            i++;
        }
        else
        {
            array[k] = rightArray[j];
            j++;
        }
        k++;
    }

    while (i < n1)
    {
        array[k] = leftArray[i];
        i++;
        k++;
    }

    while (j < n2)
    {
        array[k] = rightArray[j];
        j++;
        k++;
    }
}

static void MergeSortAlgorithm(int[] array, int left, int right)
{
    if (left < right)
    {
        int middle = left + (right - left) / 2;
        MergeSortAlgorithm(array, left, middle);
        MergeSortAlgorithm(array, middle + 1, right);
        Merge(array, left, middle, right);
    }
}